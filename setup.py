"""
Flask-Shopify
-------------

"""
from setuptools import setup, find_packages
import os
import re
import ast


def long_description():
    BASE_PATH = os.path.dirname(__file__)
    with open(os.path.join(BASE_PATH, "README.md"), encoding='utf-8') as fh:
        return fh.read()


def get_version(file):
    _version_re = re.compile(r'__version__\s+=\s+(.*)')
    with open(file, 'rb') as f:
        version = str(ast.literal_eval(_version_re.search(
            f.read().decode('utf-8')).group(1)))
        return version



setup(
    name='Flask-Shopify',
    version=get_version('flask_shopify/version.py'),
    url='https://gitlab.com/slippers/flask-shopify',
    license='BSD',
    author='Fulfil.IO Inc.',
    author_email='hello@fulfil.io',
    description='Shopify Flask',
    long_description=long_description(),
    long_description_content_type='text/markdown',
    py_modules=['flask_shopify'],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=['Flask', 'ShopifyApi', 'pymemcache'],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
