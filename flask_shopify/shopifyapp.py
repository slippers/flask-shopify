import shopify
from .config import config


class ShopifyApp(object):
    """
    SHOPIFY_SHARED_SECRET   = Your app's shared secret key with Shopify.
    SHOPIFY_API_KEY         = Your app's API key.
    SHOPIFY_SCOPES          = Array of scopes
    SHOPIFY_API_VERSION     = Shopify api version
    """

    def __init__(self):
        self.shopify_api = shopify

    def setup(self, app, connection=None):
        if not app.config['SHOPIFY_SHARED_SECRET']:
            raise Exception('SHOPIFY_SHARED_SECRET')

        if not app.config['SHOPIFY_API_KEY']:
            raise Exception('SHOPIFY_API_KEY')

        if not app.config['SHOPIFY_SCOPES']:
            raise Exception('SHOPIFY_SCOPES')

        if connection:
            self.shopify_api.base.ShopifyConnection = connection

        self.shopify_api.Session.setup(
            api_key=app.config['SHOPIFY_API_KEY'],
            secret=app.config['SHOPIFY_SHARED_SECRET']
        )
        self.scopes = app.config.get('SHOPIFY_SCOPES', [])
        self.api_version = app.config.get(
            'SHOPIFY_API_VERSION',
            config.DEFAULT_SHOPIFY_API_VERSION
        )

    def session(self, shop_token):
        """shop_token = (domain, token)
        """
        if shop_token is None:
            return None
        domain, token = shop_token
        shop_session = shopify.Session(domain, version=self.api_version, token=token)
        self.shopify_api.ShopifyResource.activate_session(shop_session)
        return shop_session

    def permission_url(self, shop_subdomain, redirect_uri):
        shop_session = self.shopify_api.Session(
            shop_subdomain,
            version=self.api_version
        )
        url = shop_session.create_permission_url(
            self.scopes, redirect_uri
        )
        return url

    def activate_session(self, shop_subdomain, params):
        session = self.shopify_api.Session(
            shop_subdomain,
            version=self.api_version
        )
        token = session.request_token(params)
        self.shopify_api.ShopifyResource.activate_session(session)
        return session, token

    def temp(self, shop_token):
        domain, token = shop_token
        return self.shopify_api.Session.temp(domain, self.api_version, token)

    def clear_session(self):
        self.shopify_api.ShopifyResource.clear_session()
