"""
https://stackoverflow.com/questions/40208849/rate-limit-api-calls-to-shopify-api-with-django-on-google-app-engine
"""
import datetime
from datetime import date, timedelta
from time import sleep
from time import sleep
from pymemcache.client.base import Client
from pyactiveresource.activeresource import formats
from pyactiveresource.connection import (
    Connection,
    ConnectionError,
    ServerError,
)
import logging


class ShopifyConnection(Connection, object):
    response = None
    SHOPIFY_MAX_RETRIES = 10
    SHOPIFY_RETRY_WAIT_SEC = 2
    SHOPIFY_TOKEN_CAPACITY = 40
    SHOPIFY_MAX_RATE = 1.95
    SHOPIFY_MIN_INTERVAL = 1
    MEMCACHE_CLIENT = ('127.0.0.1', 11211)

    def __init__(self, site, user=None, password=None, timeout=None,
                 format=formats.JSONFormat):
        super(ShopifyConnection, self).__init__(site, user, password, timeout, format)
        self.logger = logging.getLogger(__name__)
        self.client = Client(self.MEMCACHE_CLIENT)

        self.logger.info('MEMCACHE_CLIENT:%s', self.MEMCACHE_CLIENT)
        self.logger.info('SHOPIFY_MAX_RETRIES:%s', self.SHOPIFY_MAX_RETRIES)
        self.logger.info('SHOPIFY_RETRY_WAIT_SEC:%s', self.SHOPIFY_RETRY_WAIT_SEC)
        self.logger.info('SHOPIFY_TOKEN_CAPACITY:%s', self.SHOPIFY_TOKEN_CAPACITY)
        self.logger.info('SHOPIFY_MAX_RATE:%s', self.SHOPIFY_MAX_RATE)
        self.logger.info('SHOPIFY_MIN_INTERVAL:%s', self.SHOPIFY_MIN_INTERVAL)

    def consume_token(self, uid, capacity, rate, min_interval=0):
        # Get this users last UID
        last_call_time = self.client.get(uid+"_last_call_time")
        last_call_value = self.client.get(uid+"_last_call_value")
        self.logger.info('consume_token last_call:%s %s', last_call_time, last_call_value)

        if last_call_time and last_call_value:
            # Calculate how many tokens are regenerated
            now = datetime.datetime.utcnow()
            delta = rate * ((now - last_call_time).seconds)

            # If there is no change in time then check what last call value was
            if delta == 0:
                tokensAvailable = min(capacity, capacity - last_call_value)
            # If there was a change in time, how much regen has occurred
            else:
                tokensAvailable = min(capacity, (capacity - last_call_value) + delta)

            # No tokens available can't call
            if tokensAvailable <= min_interval:
                raise ConnectionError(message= "No tokens available for: " + str(uid))

    def ensure_token(self, uid):
        # Set the memcache reference
        token = {}
        token["_last_call_time"] = datetime.datetime.strptime(
            self.response.headers['Date'], '%a, %d %b %Y %H:%M:%S %Z')
        token["_last_call_value"] = int(self.response.headers['X-Shopify-Shop-Api-Call-Limit']
                                        .split('/',1)[0])
        self.logger.info('ensure_token:%s', token)
        self.client.set_multi({uid: token}, expire=25)

    def _open(self, *args, **kwargs):
        uid = self.site.split("https://")[-1].split(".myshopify.com")[0]
        self.response = None
        retries = 0
        while True:
            try:
                self.consume_token(uid, self.SHOPIFY_TOKEN_CAPACITY, self.SHOPIFY_MAX_RATE, self.SHOPIFY_MIN_INTERVAL)
                self.response = super(ShopifyConnection, self)._open(*args, **kwargs)
                self.ensure_token(uid)
                return self.response
            except (ConnectionError, ServerError) as err:
                self.logger.exception(err)
                retries += 1
                if retries > self.SHOPIFY_MAX_RETRIES:
                    self.response = err.response
                    raise
                sleep(self.SHOPIFY_RETRY_WAIT_SEC)


def configure_shopify_connection(shopify):
    """example configuration handler
    """
    ShopifyConnection.SHOPIFY_MAX_RETRIES = 10
    ShopifyConnection.SHOPIFY_RETRY_WAIT_SEC = 2
    ShopifyConnection.SHOPIFY_MIN_INTERVAL = 1

    shopify.base.ShopifyConnection = ShopifyConnection
