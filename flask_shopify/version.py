"""Version information for Flask-Shopify.
This file is imported by ``flask_shopify.__init__``, and parsed by
``setup.py`` as well as ``docs/conf.py``.
"""

from __future__ import absolute_import, print_function

# Do not change the format of this next line. Doing so risks breaking
# setup.py and docs/conf.py

__version__ = "0.2.1"
