import shopify
from flask import (
    request,
    redirect,
    url_for,
    _request_ctx_stack,
    session,
    current_app,
    abort
)
from .shopifyapp import ShopifyApp



class FlaskShopifyExt(ShopifyApp):
    """
    Shopify flask extension

    Required Flask settings::

        SHOPIFY_SHARED_SECRET
        SHOPIFY_API_KEY

    Configuring::

        ```
        from flask_shopify import Shopify

        app = Flask(__name__)
        shopify = Shopify(app)

        # Set the login view if you plan on using shopify_login_required
        # decorator
        shopify.login_view = 'auth.login'

    """

    def __init__(self, app=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tokengetter_func = self._session_token_getter
        self.tokensetter_func = self._session_token_setter
        self.login_view = None
        if app is not None:
            self.init_app(self.app)

    def init_app(self, app, connection=None):
        app.shopify = self
        self.setup(app, connection)
        app.before_request(self.before_request)

    def before_request(self):
        """
        Add the shopify_session to the request if possible.
        If there is no token, shopify_session is set to None.
        """
        shop_token = self.tokengetter_func()
        ctx = _request_ctx_stack.top
        ctx.request.shopify_session = self.session(self.tokengetter_func())

    def install(self, shop_subdomain, scopes=None, redirect_uri=None):
        """Returns a redirect response to the "permission" URL with
        the given shop. This will then prompt the user to install the app
        which will then send them to the welcome view.

        :param url: myshopify.com subdomain.
        :type url: str.
        """
        return redirect(self.permission_url(shop_subdomain, redirect_uri))

    def authenticate(self):
        session, token =  self.activate_session(
            request.args['shop'], request.args
        )
        self.tokensetter_func(request.args['shop'], token)
        return session, token

    def token_getter(self, f):
        """Registers a function as tokengetter.  The tokengetter has to return
        a tuple of ``(url, token)`` with the user's token and secret.
        If the data is unavailable, the function must return `None`.
        """
        self.tokengetter_func = f
        return f

    def token_setter(self, f):
        """Registers a function as tokensetter.  The tokensetter will be sent
        the shop and the token.
        """
        self.tokensetter_func = f
        return f

    @classmethod
    def _session_token_getter(cls):
        try:
            return session['SHOPIFY_SHOP'], session['SHOPIFY_TOKEN']
        except KeyError:
            return None

    @classmethod
    def _session_token_setter(cls, shop, token):
        session['SHOPIFY_SHOP'] = shop
        session['SHOPIFY_TOKEN'] = token

    def logout(self):
        session.pop('SHOPIFY_SHOP', None)
        session.pop('SHOPIFY_TOKEN', None)
        self.clear_session()
