from functools import wraps
from flask import (
    request, redirect, url_for, _request_ctx_stack, session,
    current_app, abort
)



def assert_shop(func):
    """
    Ensure that the shop in the session is the same as the shop in a landing
    page where something like an admin link is used.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        if request.shopify_session.url == request.args.get('shop'):
            return func(*args, **kwargs)
        else:
            current_app.shopify.logout()
            if current_app.shopify.login_view:
                return redirect(
                    url_for(
                        current_app.shopify.login_view,
                        shop=request.args.get('shop')
                    )
                )
            else:
                abort(403)
    return wrapper


def shopify_login_required(func):
    """
    Ensure that there is a login token in the session.
    """
    @wraps(func)
    def decorated_view(*args, **kwargs):
        shop_n_token = current_app.shopify.tokengetter_func()
        if not shop_n_token:
            return redirect(url_for(
                current_app.shopify.login_view,
                shop=request.args.get('shop'),
                next=request.url
            ))

        with current_app.shopify.temp(shop_n_token):
            return func(*args, **kwargs)

    return decorated_view
